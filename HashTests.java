/*
УДАЛЕНИЕ ВСЕХ ПОВТОРОВ ВКЛЮЧАЯ ОБРАЗЦЫ ПОВТОРОВ.
 */

package com.javarush.test.level08.lesson08.task05;

import java.util.*;

public class HashTests {

    public static HashMap<String, String> createMap() {

        HashMap<String, String> people = new HashMap<String, String>();

        people.put("Сосна","Игорь");
        people.put("Петров","Вадим");
        people.put("Невский","Вадим");
        people.put("Антоний","Марк");
        people.put("Цезарь","Юлий");


        return people;
    }

    public static HashMap<String, String> removeTheFirstNameDuplicates(HashMap<String, String> map) {
        Map<String, String> duplicates = new HashMap<String, String>();
        List<String> keys = new ArrayList<String>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (duplicates.containsKey(entry.getValue())) {
                keys.add(entry.getKey());
                String val = duplicates.get(entry.getValue());
                if (!keys.contains(val)) {
                    keys.add(val);
                }
            } else {
                duplicates.put(entry.getValue(), entry.getKey());
            }
        }
        for(String key : keys) {
            map.remove(key);
        }

        return map;

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet())
        {
            if (pair.getValue().equals(value))   // если совпадают значения, то удаляется текущий элемент
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println(removeTheFirstNameDuplicates(createMap()));
    }
}
